// Create a file aws-credentials.json next to aws-upload.conf.js and add the following:
//   {
//     "accessKeyId": "<access_key_id>",
//     "secretAccessKey": "<secret_access_key>",
//     "region": "us-east-1"
//   }
// Ask tech lead for access key id and secret.

var process = require('process');
process.chdir('./build');

var bucketName = "infonet-wayfinding-" + (process.env.npm_config_env || 'dev');

console.log('Deploying to AWS S3 Bucket: ' + bucketName);

module.exports = {
  credentials: "../aws-credentials.json",
  bucketName: bucketName,
  patterns:[
    "**"
  ]
};
