import {
  SET_JIBESTREAM_LOADING,
  SET_FLOOR,
  SET_IS_KIOSK,
  SET_DIRECTION_TEXT,
  SET_TEXT_DIRECTIONS,
  SET_POINT_PATH_INDEX,
  SET_SHOW_STARTING_POINT_ROOM_SEARCH,
  SET_SHOW_STARTING_POINT_PARKING_SELECTOR,
  SET_SHOW_STARTING_POINT_ENTRANCE_SELECTOR,
  SET_SHOW_STARTING_POINT_SAVED_LOCATIONS,
  SET_STARTING_SEARCH_RESULTS_SAVED_LOCATIONS,
  SET_STARTING_POINT,
  SET_STARTING_POINT_ROOM,
  SET_STARTING_SEARCH_RESULT,
  SET_DIRECTION_PATH_INDEX,
  SET_DISPLAY_WELCOME_SCREEN,
  SET_DISPLAY_STARTING_POINT_SELECTOR,
  SET_SEARCH_FILTER_FLOOR_ID,
  CATEGORY_SEARCH,
  STARTING_CATEGORY_SEARCH,
  RESTROOM_SEARCH,
  NEAREST_DESTINATION_SEARCH,
  ROOM_SEARCH,
  SET_SEARCH_RESULTS,
  RESET_SEARCH,
  HIGHLIGHT_WAYPOINTS,
  WAYFIND,
  SET_EXPAND_NAV,
  SET_EXPAND_DIRECTIONS,
  SET_EXPAND_ROOM_SEARCH,
  RESET_RESTROOM_SEARCH,
  RESET_DIRECTIONS,
  SET_WAYFIND_DEST,
  SET_USE_ELEVATORS,
} from './actions';

function reducers(state, action) {

  switch (action.type) {

    case SET_JIBESTREAM_LOADING:
      return {...state, LOADING_JIBESTREAM: action.payload};

    case SET_FLOOR:
      return {...state, CURRENT_FLOOR_ID: action.payload};

    case SET_IS_KIOSK:
      return {...state, IS_KIOSK: action.payload};

    case SET_DIRECTION_TEXT:
      return {...state, DIRECTION_TEXT: action.payload};

    case SET_POINT_PATH_INDEX:
      return {...state, POINT_PATH_INDEX: action.payload};

    case SET_DIRECTION_PATH_INDEX:
      return {...state, DIRECTION_PATH_INDEX: action.payload};

    case SET_TEXT_DIRECTIONS:
      return {...state, TEXT_DIRECTIONS: action.payload};

    case SET_DISPLAY_WELCOME_SCREEN:
      return {...state, DISPLAY_WELCOME_SCREEN: action.payload};

    case SET_DISPLAY_STARTING_POINT_SELECTOR:
      return {...state, DISPLAY_STARTING_POINT_SELECTOR: action.payload};

    case SET_SEARCH_FILTER_FLOOR_ID:
      return {...state, SEARCH_FILTER_FLOOR_ID: action.payload};

    case SET_SEARCH_RESULTS:
      return {...state, SEARCH_RESULTS: action.payload};

    case HIGHLIGHT_WAYPOINTS:
      return {...state, HIGHLIGHTED_WAYPOINTS: action.payload};

    case SET_USE_ELEVATORS:
      return {...state, USE_ELEVATORS: action.payload};

    case SET_WAYFIND_DEST:
      return {...state, WAYFIND_DEST: action.payload};

    case WAYFIND:
      return {
        ...state,
        WAYFIND_DEST: action.payload.dest,
        TEXT_DIRECTIONS: action.payload.textDirections,
        DIRECTION_TEXT: action.payload.textDirectionsOutput,
        CURRENT_FLOOR_ID: action.payload.currentFloorId,
        FLOORS_WITH_WAYFINDING_PATH: action.payload.floorsWithWayfindingPath
      };

    case CATEGORY_SEARCH:
      return {
        ...state,
        SEARCH_CATEGORY: action.payload,
        SEARCH_RESULTS: action.payload.searchResults, //TODO smarter state structure
        HIGHLIGHTED_WAYPOINTS: action.payload.highlightedWaypoints //TODO smarter state structure
      };

    case STARTING_CATEGORY_SEARCH:
      return {
        ...state,
        STARTING_SEARCH_RESULTS: action.payload.startingSearchResults, //TODO smarter state structure
      };

    case SET_STARTING_SEARCH_RESULTS_SAVED_LOCATIONS:
      return {
        ...state,
        STARTING_SEARCH_RESULTS: action.payload //TODO smarter state structure
      };

    case RESTROOM_SEARCH:
      return {
        ...state,
        SEARCH_CATEGORY: action.payload,
        RESTROOM_SEARCH: action.payload.restroomSearched
      };

    case NEAREST_DESTINATION_SEARCH:
      return {
        ...state,
        SEARCH_CATEGORY: action.payload
      };

    case ROOM_SEARCH:
      return {
        ...state,
        SEARCHED_ROOM: action.payload.roomId
      };

    case SET_EXPAND_DIRECTIONS:
      return {
        ...state,
        MAIN_HEIGHT: action.payload.mainHeight,
        NAV_HEIGHT: action.payload.navHeight,
        DIRECTIONS_EXPANDED: action.payload.directionsExpanded
      };

    case SET_EXPAND_ROOM_SEARCH:
      return {
        ...state,
        MAIN_HEIGHT: action.payload.mainHeight,
        NAV_HEIGHT: action.payload.navHeight,
        ROOM_SEARCH_EXPANDED: action.payload.roomSearchExpanded
      };

    case SET_EXPAND_NAV:
      return {
        ...state,
        MAIN_HEIGHT: action.payload.mainHeight,
        NAV_HEIGHT: action.payload.navHeight,
        NAV_EXPANDED: action.payload.navExpanded
      };

    case RESET_DIRECTIONS:
      return {
        ...state,
        TEXT_DIRECTIONS: action.payload.textDirections,
        DIRECTION_TEXT: action.payload.directionText,
        DIRECTION_PATH_INDEX: action.payload.directionPathIndex
      };

    case RESET_SEARCH:
      return {
        ...state,
        SEARCH_RESULTS: action.payload.searchResults,
        HIGHLIGHTED_WAYPOINTS: action.payload.highlightedWaypoints,
        SEARCH_CATEGORY: action.payload.searchCategory,
        SEARCHED_ROOM: action.payload.searchedRoom,
        SEARCH_FILTER_FLOOR_ID: action.payload.searchFilterFloorId
      };

    case RESET_RESTROOM_SEARCH:
      return {
        ...state,
        RESTROOM_SEARCH: action.payload.restroomSearch
      };

    case SET_SHOW_STARTING_POINT_ROOM_SEARCH:
      return {...state, SHOW_STARTING_POINT_ROOM_SEARCH: action.payload};

    case SET_SHOW_STARTING_POINT_PARKING_SELECTOR:
      return {...state, SHOW_STARTING_POINT_PARKING_SELECTOR: action.payload};

    case SET_SHOW_STARTING_POINT_ENTRANCE_SELECTOR:
      return {...state, SHOW_STARTING_POINT_ENTRANCE_SELECTOR: action.payload};

    case SET_SHOW_STARTING_POINT_SAVED_LOCATIONS:
      return {...state, SHOW_STARTING_POINT_SAVED_LOCATIONS: action.payload};

    case SET_STARTING_SEARCH_RESULT:
      return {...state, STARTING_SEARCH_RESULT: action.payload};

    case SET_STARTING_POINT_ROOM:
      return {
        ...state,
        DISPLAY_STARTING_POINT_SELECTOR: false,
        CURRENT_FLOOR_ID: action.payload,
      };

    case SET_STARTING_POINT:
      return {
        ...state,
        DISPLAY_STARTING_POINT_SELECTOR: false,
        CURRENT_FLOOR_ID: action.payload,
      };

    default:
      return state;

  }
}

export default reducers;
