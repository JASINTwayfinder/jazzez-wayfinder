import jmap from 'jmap';
import CONFIG from 'config';
import store from './store';

export const SET_JIBESTREAM_LOADING = 'SET_JIBESTREAM_LOADING';
export const ACTION_SET_JIBESTREAM_LOADING = (isLoading) => {
  return {
    type: SET_JIBESTREAM_LOADING,
    payload: isLoading
  };
};

export const SET_FLOOR = 'SET_FLOOR';
export const ACTION_SET_FLOOR = (floorId) => {
  window.BUILDING.showFloor(floorId);
  return {
    type: SET_FLOOR,
    payload: floorId
  };
};

export const SET_IS_KIOSK = 'SET_IS_KIOSK';
export const ACTION_SET_IS_KIOSK = (isKiosk) => {
  return {
    type: SET_IS_KIOSK,
    payload: isKiosk
  };
};

export const SET_DIRECTION_TEXT = 'SET_DIRECTION_TEXT';
export const ACTION_SET_DIRECTION_TEXT = (floorId) => {
  return {
    type: SET_DIRECTION_TEXT,
    payload: floorId
  };
};

export const SET_TEXT_DIRECTIONS = 'SET_TEXT_DIRECTIONS';
export const ACTION_SET_TEXT_DIRECTIONS = (directions) => {
  return {
    type: SET_TEXT_DIRECTIONS,
    payload: directions
  };
};

export const SET_DIRECTION_PATH_INDEX = 'SET_DIRECTION_PATH_INDEX';
export const ACTION_SET_DIRECTION_PATH_INDEX = (directionPathIndex) => {
  return {
    type: SET_DIRECTION_PATH_INDEX,
    payload: directionPathIndex
  };
};

export const SET_POINT_PATH_INDEX = 'SET_POINT_PATH_INDEX';
export const ACTION_SET_POINT_PATH_INDEX = (pointPathIndex) => {
  return {
    type: SET_POINT_PATH_INDEX,
    payload: pointPathIndex
  };
};

export const SET_DISPLAY_WELCOME_SCREEN = 'SET_DISPLAY_WELCOME_SCREEN';
export const ACTION_SET_DISPLAY_WELCOME_SCREEN = (isDisplaying) => {
  return {
    type: SET_DISPLAY_WELCOME_SCREEN,
    payload: isDisplaying
  };
};

export const SET_DISPLAY_STARTING_POINT_SELECTOR = 'SET_DISPLAY_STARTING_POINT_SELECTOR';
export const ACTION_SET_DISPLAY_STARTING_POINT_SELECTOR = (isDisplaying) => {
  return {
    type: SET_DISPLAY_STARTING_POINT_SELECTOR,
    payload: isDisplaying
  };
};

export const SET_SEARCH_FILTER_FLOOR_ID = 'SET_SEARCH_FILTER_FLOOR_ID';
export const ACTION_SET_SEARCH_FILTER_FLOOR_ID = (floorId) => {
  return {
    type: SET_SEARCH_FILTER_FLOOR_ID,
    payload: floorId
  };
};

export const CATEGORY_SEARCH = 'CATEGORY_SEARCH';
export const ACTION_CATEGORY_SEARCH = (category, secondaryCategories = []) => {
  var waypointResults = category_search(category, secondaryCategories);
  return {
    type: CATEGORY_SEARCH,
    payload: {
      category,
      searchResults: waypointResults,
      highlightedWaypoints: waypointResults
    }
  };
};

export const RESTROOM_SEARCH = 'RESTROOM_SEARCH';
export const ACTION_RESTROOM_SEARCH = (category) => {
  var numOfResults = 3;

  var amenity = jmap.getObjectsInArrayByString(
    [...jmap.getAmenities(), ...jmap.getDestinations()],
    category,
    CONFIG.HIGH_RANKING_PROPERTIES,
    numOfResults
  )[0];
  var destFloor = jmap.getFloorDataByMapId(window.BUILDING.destYah.mapId).id;
  var reducer = (prev, item) => {
    var itemFloor = jmap.getFloorDataByMapId(item.mapId).id;
    return itemFloor === destFloor ? prev.concat(item) : prev;
  };
  var closestWp = window.BUILDING.getClosestWaypointInArrayToWaypoint(amenity.waypoint.reduce(reducer, []), window.BUILDING.destYah, false);
  window.BUILDING.hideShowAmenities(false);
  window.document.getElementById(category + '-' + closestWp.id).style.display = "initial"; //TODO: Check if it's possible to integrate this with jibestream
  store.dispatch(ACTION_WAYFIND(closestWp));

  return {
    type: RESTROOM_SEARCH,
    payload: {
      category,
      restroomSearched: true
    }
  };
};

export const NEAREST_DESTINATION_SEARCH = 'NEAREST_DESTINATION_SEARCH';
export const ACTION_NEAREST_DESTINATION_SEARCH = (category) => {
  var numOfResults = 3;

  var dest = jmap.getObjectsInArrayByString(
    [...jmap.getAmenities(), ...jmap.getDestinations()],
    category,
    CONFIG.HIGH_RANKING_PROPERTIES,
    numOfResults
  )[0];

  store.dispatch(ACTION_WAYFIND(dest.wp));

  return {
    type: NEAREST_DESTINATION_SEARCH,
    payload: {
      category
    }
  };
};

export const STARTING_CATEGORY_SEARCH = 'STARTING_CATEGORY_SEARCH';
export const ACTION_STARTING_CATEGORY_SEARCH = (category, secondaryCategories = []) => {
  var waypointResults = category_search(category, secondaryCategories);
  return {
    type: STARTING_CATEGORY_SEARCH,
    payload: {
      category,
      startingSearchResults: waypointResults,
    }
  };
};

export const ROOM_SEARCH = 'ROOM_SEARCH';
export const ACTION_ROOM_SEARCH = (destId) => {
  var room = jmap.getDestinations().filter((dest) => dest.id === destId)[0];

  store.dispatch(ACTION_WAYFIND(room.wp));

  return {
    type: ROOM_SEARCH,
    payload: {
      roomId: destId
    }
  };
};

export const SET_EXPAND_ROOM_SEARCH = 'SET_EXPAND_ROOM_SEARCH';
export const ACTION_SET_EXPAND_ROOM_SEARCH = (isShown) => {
  var mainHeight = isShown ? CONFIG.MAIN_HEIGHT_EXPANDED : CONFIG.MAIN_HEIGHT_COLLAPSED;
  var navHeight = isShown ? CONFIG.NAV_HEIGHT_EXPANDED : CONFIG.NAV_HEIGHT_COLLAPSED;
  return {
    type: SET_EXPAND_ROOM_SEARCH,
    payload: {
      mainHeight: mainHeight,
      navHeight: navHeight,
      roomSearchExpanded: isShown
    }
  };
};

export const SET_SEARCH_RESULTS = 'SET_SEARCH_RESULTS';
export const ACTION_SET_SEARCH_RESULTS = (results) => {
  return {
    type: SET_SEARCH_RESULTS,
    payload: results
  };
};

export const SET_STARTING_SEARCH_RESULTS_SAVED_LOCATIONS = 'SET_STARTING_SEARCH_RESULTS_SAVED_LOCATIONS';
export const ACTION_SET_STARTING_SEARCH_RESULTS_SAVED_LOCATIONS = () => {
  var searchResults = JSON.parse(localStorage.getItem("savedLocations"));
  return {
    type: SET_STARTING_SEARCH_RESULTS_SAVED_LOCATIONS,
    payload: searchResults
  };
};

export const RESET_DIRECTIONS = 'RESET_DIRECTIONS';
export const ACTION_RESET_DIRECTIONS = () => {
  resetMaps();
  return {
    type: RESET_DIRECTIONS,
    payload: {
      textDirections: [],
      directionText: '',
      directionPathIndex: 0
    }
  };
};

export const RESET_SEARCH = 'RESET_SEARCH';
export const ACTION_RESET_SEARCH = () => {
  resetMaps();
  return {
    type: RESET_SEARCH,
    payload: {
      searchResults: [],
      highlightedWaypoints: [],
      searchCategory: '',
      searchedRoom: '',
      searchFilterFloorId: null
    }
  };
};

export const RESET_RESTROOM_SEARCH = 'RESET_RESTROOM_SEARCH';
export const ACTION_RESET_RESTROOM_SEARCH = () => {
  resetMaps();
  return {
    type: RESET_RESTROOM_SEARCH,
    payload: {
      restroomSearch: false
    }
  }
};

export const HIGHLIGHT_WAYPOINTS = 'HIGHLIGHT_WAYPOINTS';
export const ACTION_HIGHLIGHT_WAYPOINTS = () => {
  return {
    type: HIGHLIGHT_WAYPOINTS,
    payload: null
  }
};

export const SET_EXPAND_NAV = 'SET_EXPAND_NAV';
export const ACTION_SET_EXPAND_NAV = (isExpanded) => {
  var mainHeight = isExpanded ? CONFIG.MAIN_HEIGHT_EXPANDED : CONFIG.MAIN_HEIGHT_COLLAPSED;
  var navHeight = isExpanded ? CONFIG.NAV_HEIGHT_EXPANDED : CONFIG.NAV_HEIGHT_COLLAPSED;

  return {
    type: SET_EXPAND_NAV,
    payload: {
      mainHeight: mainHeight,
      navHeight: navHeight,
      navExpanded: isExpanded
    }
  };
};

export const SET_EXPAND_DIRECTIONS = 'SET_EXPAND_DIRECTIONS';
export const ACTION_SET_EXPAND_DIRECTIONS = (isExpanded) => {
  return {
    type: SET_EXPAND_DIRECTIONS,
    payload: {
      directionsExpanded: isExpanded,
      mainHeight: CONFIG.MAIN_HEIGHT_COLLAPSED,
      navHeight: CONFIG.NAV_HEIGHT_COLLAPSED
    }
  };
};

export const SET_VISIBLE_AMENITIES = 'SET_VISIBLE_AMENITIES';
export const ACTION_SET_VISIBLE_AMENITIES = () => {
  window.BUILDING.hideShowAmenities(false);
  jmap.getObjectsInArrayByString(jmap.getAmenities(), 'restroom atm', ['keywords'], 2)
    .forEach((item) => {
      window.BUILDING.showAmenityById(item.componentId, true);
    }
  );

  return {
    type: SET_VISIBLE_AMENITIES,
    payload: null
  }
};

export const SET_SHOW_STARTING_POINT_ROOM_SEARCH = 'SET_SHOW_STARTING_POINT_ROOM_SEARCH';
export const ACTION_SET_SHOW_STARTING_POINT_ROOM_SEARCH = (show) => {
  return {
    type: SET_SHOW_STARTING_POINT_ROOM_SEARCH,
    payload: show
  };
};

export const SET_SHOW_STARTING_POINT_PARKING_SELECTOR = 'SET_SHOW_STARTING_POINT_PARKING_SELECTOR';
export const ACTION_SET_SHOW_STARTING_POINT_PARKING_SELECTOR = (show) => {
  return {
    type: SET_SHOW_STARTING_POINT_PARKING_SELECTOR,
    payload: show
  };
};

export const SET_SHOW_STARTING_POINT_ENTRANCE_SELECTOR = 'SET_SHOW_STARTING_POINT_ENTRANCE_SELECTOR';
export const ACTION_SET_SHOW_STARTING_POINT_ENTRANCE_SELECTOR = (show) => {
  return {
    type: SET_SHOW_STARTING_POINT_ENTRANCE_SELECTOR,
    payload: show
  };
};

export const SET_SHOW_STARTING_POINT_SAVED_LOCATIONS = 'SET_SHOW_STARTING_POINT_SAVED_LOCATIONS';
export const ACTION_SET_SHOW_STARTING_POINT_SAVED_LOCATIONS = (show) => {
  return {
    type: SET_SHOW_STARTING_POINT_SAVED_LOCATIONS,
    payload: show
  };
};

export const SET_STARTING_SEARCH_RESULT = 'SET_STARTING_SEARCH_RESULT';
export const ACTION_SET_STARTING_SEARCH_RESULT = (result) => {
  return {
    type: SET_STARTING_SEARCH_RESULT,
    payload: result
  };
};

export const SET_STARTING_POINT_ROOM = 'SET_STARTING_POINT_ROOM';
export const ACTION_SET_STARTING_POINT_ROOM = (destId, name, save) => {
  var room = jmap.getDestinations().filter((dest) => dest.id === destId)[0];
  window.BUILDING.destYah = room.wp;
  window.BUILDING.setYah();
  window.BUILDING.focusToYah(CONFIG.ZOOM_LEVEL);
  if (save){
    var waypoint = room.wp;
    waypoint.name = name;
    store.dispatch(ACTION_SAVE_WAYPOINT(waypoint));
  }
  var floorId = jmap.getFloorDataByMapId(room.wp.mapId).id;
  store.dispatch(ACTION_SET_FLOOR(floorId));
  return {
    type: SET_STARTING_POINT_ROOM,
    payload: floorId
  };
};

export const SET_STARTING_POINT = 'SET_STARTING_POINT';
export const ACTION_SET_STARTING_POINT = (waypoint, save) => {
  window.BUILDING.destYah = waypoint;
  window.BUILDING.setYah();
  window.BUILDING.focusToYah(CONFIG.ZOOM_LEVEL);
  if(save){
    store.dispatch(ACTION_SAVE_WAYPOINT(waypoint));
  }
  var floorId = jmap.getFloorDataByMapId(waypoint.mapId).id;
  store.dispatch(ACTION_SET_FLOOR(floorId));
  return {
    type: SET_STARTING_POINT,
    payload: floorId
  };
};

export const SET_WAYFIND_DEST = 'SET_WAYFIND_DEST';
export const ACTION_SET_WAYFIND_DEST = (waypoint) => {
  return {
    type: SET_WAYFIND_DEST,
    payload: waypoint
  }
};

export const SET_USE_ELEVATORS = 'SET_USE_ELEVATORS';
export const ACTION_SET_USE_ELEVATORS = (useElevators) => {
  return {
    type: SET_USE_ELEVATORS,
    payload: useElevators
  }
};

export const SAVE_WAYPOINT = 'SAVE_WAYPOINT';
export const ACTION_SAVE_WAYPOINT = (waypoint) => {
  var savedLocations = JSON.parse(localStorage.getItem("savedLocations"));
  if(savedLocations === null){
    savedLocations = [];
  }
  if(savedLocations.length >= 10){
    savedLocations.shift()
  }
  savedLocations.push(waypoint);
  localStorage.setItem("savedLocations", JSON.stringify(savedLocations));
  return {
    type: SAVE_WAYPOINT,
    payload: null
  }
};

export const WAYFIND = 'WAYFIND';
export const ACTION_WAYFIND = (dest) => {
  cleanUpCustomIcons();

  var state = store.getState();

  var pathData = window.BUILDING.wayfind(
    window.BUILDING.destYah,
    dest,
    Object.assign(CONFIG.WAYFIND_OPTIONS, { useElevator: state.USE_ELEVATORS })
  ).pathData;
  var textDirections = [];
  pathData
    .textDirections
    .forEach((direction) => {
      textDirections.push(...direction);
    });

  window.BUILDING.focusToYah(CONFIG.ZOOM_LEVEL);

  //TODO use pathAnimationComplete event
  if (CONFIG.WAYFIND_OPTIONS.animate) {
    var counter = 0;
    var oldFloorId = window.BUILDING.currentFloor.id;
    var intervalHandler = setInterval(() => {
      if (counter > 40) {
        clearInterval(intervalHandler);
      }
      counter++;
      if (oldFloorId !== window.BUILDING.currentFloor.id) {
        store.dispatch(ACTION_SET_FLOOR(window.BUILDING.currentFloor.id));
        oldFloorId = window.BUILDING.currentFloor.id;
      }
    }, 250);
  }

  return {
    type: WAYFIND,
    payload: {
      dest: dest,
      textDirections: textDirections,
      textDirectionsOutput: textDirections[0].output,
      currentFloorId: window.BUILDING.currentFloor.id,
      floorsWithWayfindingPath: pathData.textDirections.map(e => e[0].floor)
    }
  };
};

function resetMaps() {
  cleanUpCustomIcons();
  window.BUILDING.resetAllMaps();
  //window.BUILDING.hideShowAmenities(false);
  jmap
    .getObjectsInArrayByString(jmap.getAmenities(), 'restroom atm', ['keywords'], 2)
    .forEach((item) => {
        window.BUILDING.showAmenityById(item.componentId, true);
      }
    );
  window.BUILDING.focusToYah(CONFIG.ZOOM_LEVEL);
}

function cleanUpCustomIcons() {
  var oldStepIcon = document.getElementsByClassName('step-icon')[0];
  if (oldStepIcon) {
    oldStepIcon.remove();
  }
}

function category_search(category, secondaryCategories) {
  var numOfResults = 100;
  var results1 = jmap.getObjectsInArrayByString(
    [...jmap.getAmenities(), ...jmap.getDestinations()],
    category,
    CONFIG.HIGH_RANKING_PROPERTIES,
    numOfResults
  );

  var results = [];
  if (results1.length > 0 && secondaryCategories.length > 0) {
    secondaryCategories.forEach((secondaryCategory) => {
      var secondaryResults = results1.filter((item) => {
        var isValid = false;
        if (item.keywords) {
          item.keywords.split(',').forEach((keyword) => {
            if (keyword.trim().toLowerCase() === secondaryCategory) {
              isValid = true;
            }
          })
        }
        return isValid;
      });
      results = [...results, ...secondaryResults]
    })
  } else {
    results = results1;
  }

  var waypointResults = [];
  results.forEach((result) => {
    if (typeof(result.waypoints[0]) !== "number") {
      result.waypoints.forEach((wp) => {
        var copy = Object.assign({}, wp);
        copy.name = result.label;
        waypointResults.push(copy);
      });
    }
  });

  return waypointResults.sort((a, b) => {
    if (a.name.toUpperCase() > b.name.toUpperCase()) return 1;
    if (a.name.toUpperCase() < b.name.toUpperCase()) return -1;
    return 0;
  });
}
