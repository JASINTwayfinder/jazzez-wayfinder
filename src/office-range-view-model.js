import jmap from 'jmap';

function OfficeRangeViewModel() {
  var officeRangesAndId = [...(jmap.getDestinations())]
    .filter((destination) => {
      return destination.category.indexOf('Offices') > -1;
    })
    .reduce((accum, officeDestination) => {
      var keywords = officeDestination.keywords
        .split(',')
        .reduce((accum, s) => {
          var sTrimmed = s.trim();
          if (sTrimmed === 'Office' || sTrimmed === 'Offices') {
            return accum;
          } else {
            return [...accum, sTrimmed + ':' + officeDestination.id]; //append id for later
          }
        }, []);
      return accum.concat(keywords);
    }, []);

  return [...officeRangesAndId].reduce((accumulator, rangeAndId) => {
    rangeAndId = rangeAndId.split(':');

    var pushData = true;
    var id = parseInt(rangeAndId[1], 10);
    var floor = null;
    var ring = null;
    var corridor = null;
    var officeRangeMin;
    var officeRangeMax;

    var range = rangeAndId[0].split('-');
    if (range.length === 1) {
      range.push(range[0])
    }

    //TODO remove
    if (range[1] === '55') {
      range[1] = '3B400';
    }
    if (range[0] === '3C300' && range[1] === '3B355') {
      range[1] = range[0];
    }

    var rangeSplit1 = range[0].split(';');
    var rangeSplit2 = range[1].split(';');
    if (rangeSplit1[0] === rangeSplit2[0]) {
      floor = rangeSplit1[0];
    } else {
      console.log('Floor mismatch: ' + id);
      pushData = false;
    }

    if (rangeSplit1[1] === rangeSplit2[1]) {
      ring = rangeSplit1[1];
    } else {
      console.log('Ring mismatch: ' + id);
      pushData = false;
    }

    if (rangeSplit1[2] === rangeSplit2[2]) {
      corridor = rangeSplit1[2];
    } else {
      console.log('Corridor mismatch: ' + id);
      pushData = false;
    }

    officeRangeMin = rangeSplit1[3];
    officeRangeMax = rangeSplit2[3];

    if(pushData){
      return [...accumulator, {id, floor, ring, corridor, officeRange: {min: officeRangeMin.trim(), max: officeRangeMax.trim()}}]
    } else {
      return accumulator;
    }
  }, []);

}

export default OfficeRangeViewModel;
