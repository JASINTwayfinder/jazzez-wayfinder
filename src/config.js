var config = {
  JIBESTREAM: {
    URL: 'https://pntgn-dev-vm1.js-network.co',
    DEFAULT_DEVICE_ID: 1281,
    LOCATION_ID: 324,
    API_USER: 'apiuser',
    API_PASSWORD: 'Pentagon@2017',
    API_KEY: 'AZLZ143396ic3855554TSX143396cfud',
    CODE: 'en'
  },

  HEADER_HEIGHT: '8vh',
  MAIN_HEIGHT_COLLAPSED: '78vh',
  MAIN_HEIGHT_EXPANDED: '54vh',
  NAV_HEIGHT_COLLAPSED: '14vh',
  NAV_HEIGHT_EXPANDED: '38vh',
  FOOTER_HEIGHT: '0',

  WELCOME_SCREEN_TIMEOUT: 15, // in minutes
  ZOOM_LEVEL: 2,

  WAYFIND_OPTIONS: {
    animate: false
  },
  HIGH_RANKING_PROPERTIES: ["name", "keywords"]
};

if (process.env.REACT_APP_PROD === 'true') {
  config.JIBESTREAM.URL = 'https://pntgn-prod-vm1.js-network.co'; //TODO update to default prod url
} else if (process.env.REACT_APP_PROD) {
  config.JIBESTREAM.URL = process.env.REACT_APP_PROD;
}

export default config
