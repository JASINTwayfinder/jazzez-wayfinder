import 'normalize.css';
import 'index.css';
import 'font-awesome/css/font-awesome.min.css';

import 'url-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import registerServiceWorker from 'register-service-worker';
import store from 'redux/store';

import App from 'components/App/App';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
registerServiceWorker();
