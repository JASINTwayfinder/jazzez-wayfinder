import './Nav.css';

import React, { Component } from 'react';
import QuickLinks from 'components/QuickLinks/QuickLinks';
import SearchResults from 'components/SearchResults/SearchResults';
import RoomSearch from 'components/RoomSearch/RoomSearch';
import PathNavigator from 'components/PathNavigator/PathNavigator';
import poweredByInfoNetLogo from 'assets/LogoInfoNet.svg';
import { connect } from 'react-redux';
import {
  ACTION_SET_FLOOR,
  ACTION_SET_DIRECTION_TEXT,
  ACTION_SET_POINT_PATH_INDEX,
  ACTION_SET_DIRECTION_PATH_INDEX,
  ACTION_SET_TEXT_DIRECTIONS,
  ACTION_SET_EXPAND_NAV,
  ACTION_SET_EXPAND_DIRECTIONS,
  ACTION_WAYFIND
} from 'redux/actions';

class Nav extends Component {

  render = () => {
    return (
      <div id="nav-container">
        {this.showNavComponent()}
        <div className="infonet-logo">
          <img alt="Powered by InfoNet logo" src={poweredByInfoNetLogo}/>
        </div>
      </div>
    );
  };

  componentDidMount = () => {
    window.BUILDING.addLandmarkInteraction('click touchstart', (destinationId, waypoint, event) => {
      this.wayfind(waypoint);
    });
  };

  showNavComponent = () => {
    if(this.props.navExpanded){
      return <SearchResults/>;
    } else {
      if(this.props.directionsExpanded){
        return <PathNavigator />;
      } else {
        return (this.props.roomSearchExpanded ? <RoomSearch /> : <QuickLinks />);
      }
    }
  };

  wayfind = (waypoint) => {
    this.props.ACTION_SET_POINT_PATH_INDEX(0);
    this.props.ACTION_SET_DIRECTION_PATH_INDEX(0);
    this.props.ACTION_WAYFIND(waypoint);
    this.props.ACTION_SET_EXPAND_NAV(false);
    this.props.ACTION_SET_EXPAND_DIRECTIONS(true);
  };

}

const mapStateToProps = (storeState) => {
  return {
    navExpanded: storeState.NAV_EXPANDED,
    directionsExpanded: storeState.DIRECTIONS_EXPANDED,
    roomSearchExpanded: storeState.ROOM_SEARCH_EXPANDED
  }
};

export default connect(mapStateToProps, {
  ACTION_SET_FLOOR,
  ACTION_SET_DIRECTION_TEXT,
  ACTION_SET_POINT_PATH_INDEX,
  ACTION_SET_DIRECTION_PATH_INDEX,
  ACTION_SET_TEXT_DIRECTIONS,
  ACTION_SET_EXPAND_NAV,
  ACTION_SET_EXPAND_DIRECTIONS,
  ACTION_WAYFIND
})(Nav);
