import './StartingPointEntrancesSelector.css';

import React, {Component} from 'react';
import { connect } from 'react-redux';
import StartingPointSearchResults from 'components/StartingPointSearchResults/StartingPointSearchResults';
import {
  ACTION_SET_STARTING_POINT,
  ACTION_SET_SHOW_STARTING_POINT_ENTRANCE_SELECTOR,
  ACTION_STARTING_CATEGORY_SEARCH
} from 'redux/actions';

class StartingPointParkingSelector extends Component {

  state = {
    saveDest: false
  };

  render = () => {
    return (
      <div id="starting-point-entrances-search" className='starting-point-entrances-search'>
        <div className="btn--top-left" onClick={this.exitSearch}>
          <i className="fa fa-long-arrow-left" aria-hidden="true"/> Back
        </div>
        <div className="starting-point-entrances-search-info">
          <div className="title">
            Entrances
          </div>
          <p>
            Your trip is starting from a Pentagon entrance. All Government or DoD Common Access Card, CAC holders can
            access the Pentagon facility from any entrance listed. Unsure of which to choose? Select one of the
            following most commonly used entrances to get started.<br/><br/>
            Metro Entrance | Monday-Friday 5:00am–8:00pm<br/>
            Corridor 2 Entrance | 24/7<br/>
            Corridor 3 Entrance | 24/7 – Temporarily Closed for Renovation<br/><br/>
            Contact the Pentagon Building Pass Office at (703) 693-3953 with any questions.
            Office hours are Mon., Tue., Thur., and Fri., 8 a.m.-4 p.m.or Wed. 8:30 a.m.-4 p.m.
          </p>
          <div className="save-selector">
            <div className="save-checkbox">
              <input type="checkbox"
                     className="button-checkbox"
                     name="saveDest"
                     id="saveDest"
                     checked={this.state.saveDest}
                     onClick={this.toggleSave}/>
              <label htmlFor="saveDest"> Save my starting location</label>
            </div>
          </div>
          <div className="begin">
            <div className="begin-button">
              <button disabled={this.props.selectedEntrance === null}
                      onClick={this.handleSubmit}>Begin</button>
            </div>
          </div>
        </div>
        <StartingPointSearchResults />
      </div>
    );
  };

  componentDidMount = () => {
    this.props.ACTION_STARTING_CATEGORY_SEARCH('entrance exit');
  };

  toggleSave = (event) => {
    this.setState({
      saveDest: event.target.checked
    })
  };

  handleSubmit = () => {
    this.props.ACTION_SET_STARTING_POINT(this.props.selectedEntrance, this.state.saveDest);
  };

  exitSearch = () => {
    this.props.ACTION_SET_SHOW_STARTING_POINT_ENTRANCE_SELECTOR(false);
  };
}

const mapStateToProps = (storeState) => {
  return {
    selectedEntrance: storeState.STARTING_SEARCH_RESULT
  }
};

export default connect(mapStateToProps, {
  ACTION_SET_STARTING_POINT,
  ACTION_SET_SHOW_STARTING_POINT_ENTRANCE_SELECTOR,
  ACTION_STARTING_CATEGORY_SEARCH
})(StartingPointParkingSelector);
