import './Map.css';

import React, {Component} from 'react';
import { connect } from 'react-redux';
import FloorSelector from 'components/FloorSelector/FloorSelector';


class Map extends Component {
  render = () => {
    return (
      <div id="map-container">
        {this.props.isJibestreamLoading ? '' : this.stylePeopleMoverIconsOnFloorsWithPath()}
        <div id="map"></div>
        {this.props.isJibestreamLoading ? '' : <FloorSelector/>}
      </div>
    );
  };

  stylePeopleMoverIconsOnFloorsWithPath() {
    return this.props.floorsWithWayfindingPath.map((floorId, index) => {
      return <style key={index}>{
        `
        #map-floor-${floorId} .JMap-Active-Mover img {
          animation: active_mover_icon_pulse_animation 1500ms infinite;
          transform-origin: 50% 50%;
          background: white;
          border: 1px solid #E04151;
          pointer-events: none;
        }

        .JMap-Custom-${floorId}-step-icon {
          z-index: 4;
        }

        `
      }</style>;
    });
  }
}

const mapStateToProps = (storeState) => {
  return {
    isJibestreamLoading: storeState.LOADING_JIBESTREAM,
    floorsWithWayfindingPath: storeState.FLOORS_WITH_WAYFINDING_PATH
  }
};

export default connect(mapStateToProps)(Map);
