import './StartingPointSavedLocations.css';

import React, {Component} from 'react';
import { connect } from 'react-redux'
import StartingPointSavedLocationsError from 'components/StartingPointSavedLocationsError/StartingPointSavedLocationsError';
import StartingPointSearchResults from 'components/StartingPointSearchResults/StartingPointSearchResults';

import {
  ACTION_SET_STARTING_POINT,
  ACTION_SET_SHOW_STARTING_POINT_SAVED_LOCATIONS
} from 'redux/actions';

class StartingPointSavedLocations extends Component {

  savedLocations = localStorage.getItem("savedLocations") !== null ?
    JSON.parse(localStorage.getItem("savedLocations")) : [];

  render = () => {
    if(this.savedLocations.length === 0){
      return (
        <div id="starting-point-parking-search" className='starting-point-parking-search'>
          <div className="btn--top-left" onClick={this.exitSearch}>
            <i className="fa fa-long-arrow-left" aria-hidden="true"/> Back
          </div>
          <StartingPointSavedLocationsError />
        </div>
      )
    } else {
      return (
        <div id="starting-point-parking-search" className='starting-point-parking-search'>
          <div className="btn--top-left" onClick={this.exitSearch}>
            <i className="fa fa-long-arrow-left" aria-hidden="true"/> Back
          </div>
          <div className="starting-point-parking-search-info">
            <div className="title">
              Saved Locations
            </div>
            <p>
              What’s better than something familiar? Start your trip in the Pentagon from one of your saved locations.
            </p>
            <div className="begin">
              <div className="begin-button">
                <button disabled={this.props.selectedLocation === null}
                        onClick={this.handleSubmit}>Begin</button>
              </div>
            </div>
          </div>
          <StartingPointSearchResults />
        </div>
      );
    }
  };

  componentDidMount = () => {};


  handleSubmit = () => {
    this.props.ACTION_SET_STARTING_POINT(this.props.selectedLocation);
  };

  exitSearch = () => {
    this.props.ACTION_SET_SHOW_STARTING_POINT_SAVED_LOCATIONS(false);
  };
}

const mapStateToProps = (storeState) => {
  return {
    startingLocations: storeState.STARTING_SEARCH_RESULTS,
    selectedLocation: storeState.STARTING_SEARCH_RESULT
  }
};

export default connect(mapStateToProps, {
  ACTION_SET_STARTING_POINT,
  ACTION_SET_SHOW_STARTING_POINT_SAVED_LOCATIONS
})(StartingPointSavedLocations);
