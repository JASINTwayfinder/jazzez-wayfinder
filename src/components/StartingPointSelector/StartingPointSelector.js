import './StartingPointSelector.css';

import React, {Component} from 'react';
import { connect } from 'react-redux'
import Modal from 'components/Modal/Modal';
import StartingPointSelectorDisplay from 'components/StartingPointSelectorDisplay/StartingPointSelectorDisplay';
import StartingPointRoomSearch from 'components/StartingPointRoomSearch/StartingPointRoomSearch';
import StartingPointParkingSelector from 'components/StartingPointParkingSelector/StartingPointParkingSelector';
import StartingPointEntrancesSelector from 'components/StartingPointEntrancesSelector/StartingPointEntrancesSelector';
import StartingPointSavedLocations from 'components/StartingPointSavedLocations/StartingPointSavedLocations';

class StartingPointSelector extends Component {


  render = () => {
    if (this.props.display) {
      return (
        <Modal>
          <div className="starting-point-selector">{this.showSelectorComponent()}</div>
        </Modal>
      );
    } else {
      return null;
    }
  };

  showSelectorComponent = () => {
    if (this.props.displayRoomSearch){
      return <StartingPointRoomSearch />;
    } else if (this.props.displayParkingSearch) {
      return <StartingPointParkingSelector />;
    } else if (this.props.displayEntrancesSearch){
      return <StartingPointEntrancesSelector/>
    } else if (this.props.displaySavedLocations) {
      return <StartingPointSavedLocations />
    } else {
      return <StartingPointSelectorDisplay />;
    }
  };

  componentDidMount = () => {};
}

const mapStateToProps = (storeState) => {
  return {
    display: storeState.DISPLAY_STARTING_POINT_SELECTOR,
    displayRoomSearch: storeState.SHOW_STARTING_POINT_ROOM_SEARCH,
    displayParkingSearch: storeState.SHOW_STARTING_POINT_PARKING_SELECTOR,
    displayEntrancesSearch: storeState.SHOW_STARTING_POINT_ENTRANCE_SELECTOR,
    displaySavedLocations: storeState.SHOW_STARTING_POINT_SAVED_LOCATIONS
  }
};

export default connect(mapStateToProps, {})(StartingPointSelector);
