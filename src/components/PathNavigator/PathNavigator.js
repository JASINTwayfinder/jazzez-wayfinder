import './PathNavigator.css';

import CONFIG from 'config';
import React, {Component} from 'react';
import { connect } from 'react-redux'
import ElevatorToggle from 'components/ElevatorToggle/ElevatorToggle';
import jmap from 'jmap';
import rightArrowIcon from 'assets/icons/icon-right.svg';
import leftArrowIcon from 'assets/icons/icon-left.svg';
import {
  ACTION_SET_FLOOR,
  ACTION_SET_DIRECTION_TEXT,
  ACTION_SET_DIRECTION_PATH_INDEX,
  ACTION_SET_EXPAND_DIRECTIONS,
  ACTION_SET_EXPAND_ROOM_SEARCH,
  ACTION_RESET_SEARCH,
  ACTION_RESET_RESTROOM_SEARCH,
  ACTION_RESET_DIRECTIONS

} from 'redux/actions';

class PathNavigator extends Component {

  render() {
    var dirStyle = {
      fontSize: '2.2vh'
    };
    if (this.props.currentDirection.length > 110)
      dirStyle ={
        fontSize: '1.6vh',
        lineHeight: '2.4vh'
      };
    else if (this.props.currentDirection.length > 75)
      dirStyle ={
        fontSize: '1.8vh',
        lineHeight: '2.4vh'
      };
    return (
      <div className="path-navigator">
        <div className="go-back"
             onClick={this.prevDirection}
             style={{visibility: this.showLeftArrow() ? 'visible' : 'hidden'}}
        >
          <img alt="left-arrow" src={leftArrowIcon} />
        </div>
        <div className="directions" style={dirStyle}>{this.props.currentDirection}</div>
        <div className="go-forward"
             onClick={this.nextDirection}
             style={{visibility: this.showRightArrow() ? 'visible' : 'hidden'}}
        >
          <img alt="right-arrow" src={rightArrowIcon} />
        </div>
        <div className="nav--top-right-buttons">
          {this.showToggleElevators()}
          {this.props.isKiosk ? "" : <div><i className="fa fa-print" aria-hidden="true" onClick={() => window.print()}/></div>}
          <div>
            <i className="fa fa-times-thin" aria-hidden="true" onClick={this.exitDirections}/>
          </div>
        </div>
      </div>
    );
  }

  showLeftArrow = () => {
    return this.props.currentDirectionIndex > 0;
  };

  showRightArrow = () => {
    return this.props.currentDirectionIndex < this.props.textDirections.length -1;
  };

  showToggleElevators = () => {
     var yahFloor = jmap.getFloorDataByMapId(window.BUILDING.destYah.mapId).id;
     var destFloor = jmap.getFloorDataByMapId(this.props.wayfindDest.mapId).id;
    return yahFloor ===  destFloor ?
      "" : <ElevatorToggle />;
  };

  prevDirection = () => {
    if (this.props.currentDirectionIndex <= 0) {
      return;
    }
    var index = this.props.currentDirectionIndex - 1;
    this.props.ACTION_SET_DIRECTION_PATH_INDEX(index);

    var currentDirection = this.props.textDirections[index];
    if (window.BUILDING.currentFloor.id !== currentDirection.floor) {
      this.props.ACTION_SET_FLOOR(currentDirection.floor);
    }
    window.BUILDING.focusToPoint(currentDirection.wp.x, currentDirection.wp.y, CONFIG.ZOOM_LEVEL);
    this.props.ACTION_SET_DIRECTION_TEXT(currentDirection.output);
    showStepNumber(currentDirection, index);
  };

  nextDirection = () => {
    if (this.props.currentDirectionIndex >= this.props.textDirections.length - 1) {
      return;
    }
    var index = this.props.currentDirectionIndex + 1;
    this.props.ACTION_SET_DIRECTION_PATH_INDEX(index);

    var currentDirection = this.props.textDirections[index];
    if (window.BUILDING.currentFloor.id !== currentDirection.floor) {
      this.props.ACTION_SET_FLOOR(currentDirection.floor);
    }
    window.BUILDING.focusToPoint(currentDirection.wp.x, currentDirection.wp.y, CONFIG.ZOOM_LEVEL);
    this.props.ACTION_SET_DIRECTION_TEXT(currentDirection.output);
    showStepNumber(currentDirection, index);
  };

  exitDirections = () => {
    this.props.ACTION_RESET_DIRECTIONS();
    this.props.ACTION_SET_EXPAND_DIRECTIONS(false);
    this.props.ACTION_SET_EXPAND_ROOM_SEARCH(false);
    this.props.ACTION_RESET_SEARCH();
    this.props.ACTION_SET_FLOOR(jmap.getFloorDataByMapId(window.BUILDING.destYah.mapId).id);
    this.props.ACTION_RESET_RESTROOM_SEARCH();
  }
}

function showStepNumber(currentDirection, index) {
  var oldStepIcon = document.getElementsByClassName('step-icon')[0];
  if (oldStepIcon) {
    oldStepIcon.remove();
  }

  if (index === 0) {
    return;
  }

  var style = 'transform: translate3D(-50%, -50%, 0);';
  if (currentDirection.type === 'mover' || currentDirection.type === 'end') {
    style = 'transform: translate3D(-50%, 35%, 0);'; // Show a bit below the destination or YAH icon
  }

  var stepIcon = '<div class="step-icon" style="'+ style + '">' + index + '</div>';
  window.BUILDING.showCustomPopup(currentDirection.floor, "step-icon", stepIcon, {
    x: currentDirection.wp.x,
    y: currentDirection.wp.y
  })
}

const mapStateToProps = (storeState) => {
  return {
    currentPointIndex: storeState.POINT_PATH_INDEX,
    currentDirectionIndex: storeState.DIRECTION_PATH_INDEX,
    textDirections: storeState.TEXT_DIRECTIONS,
    currentDirection: storeState.DIRECTION_TEXT,
    currentFloorID: storeState.CURRENT_FLOOR_ID,
    wayfindDest: storeState.WAYFIND_DEST,
    isKiosk: storeState.IS_KIOSK
  }
};

export default connect(mapStateToProps, {
  ACTION_SET_FLOOR,
  ACTION_SET_DIRECTION_TEXT,
  ACTION_SET_DIRECTION_PATH_INDEX,
  ACTION_SET_EXPAND_DIRECTIONS,
  ACTION_SET_EXPAND_ROOM_SEARCH,
  ACTION_RESET_SEARCH,
  ACTION_RESET_RESTROOM_SEARCH,
  ACTION_RESET_DIRECTIONS
})(PathNavigator);
