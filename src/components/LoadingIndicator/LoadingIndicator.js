import React, {Component} from 'react';

export default class LoadingIndicator extends Component {

  render = () => {
    const {value} = this.props;

    return (
      <input
        type="text"
        onInput={this.handleInput.bind(this)}
        value={value}
        onFocus={this.handleFocus.bind(this)}
        ref="input"
      />
    );
  };

}
