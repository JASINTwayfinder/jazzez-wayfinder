import './Modal.css';

import React, {Component} from 'react';

class Modal extends Component {

  render = () => {
    return (
      <div className="modal">
        <div className="backdrop"></div>
        <div className="content">
          {this.props.children}
        </div>
      </div>
    );
  };

}

export default Modal;
