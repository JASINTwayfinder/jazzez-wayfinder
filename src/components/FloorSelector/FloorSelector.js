import './FloorSelector.css';

import React, {Component} from 'react';
import { connect } from 'react-redux'
import {
  ACTION_SET_FLOOR,
  ACTION_RESET_RESTROOM_SEARCH,
  ACTION_SET_EXPAND_DIRECTIONS
} from 'redux/actions';

class FloorSelector extends Component {

  render = () => {
    return (
      <div className="floor-selector">
        {this.renderFloorButtons()}
      </div>
    );
  };

  renderFloorButtons = () => {
    //TODO using fat arrow func in onClick below has bad performance--refactor https://stackoverflow.com/questions/29810914/react-js-onclick-cant-pass-value-to-method
    return Object
      .keys(window.BUILDING.floors)
      .map(e => window.BUILDING.floors[e])
      .sort((a, b) => a.sequence - b.sequence)
      .map((floor) => {
        return (
          <a
            key={floor.id}
            onClick={() => {this.selectFloor(floor.id)}}
            className={floor.id === this.props.selectedFloorId ? 'selected' : ''}
          >
            {this.mapFloorTitleToDisplayValue(floor.floorTitle)}
          </a>
        )
      });
  };

  selectFloor = (floorId) => {
    this.props.ACTION_SET_FLOOR(floorId);
    if(this.props.restroomSearched){
      this.props.ACTION_RESET_RESTROOM_SEARCH();
      this.props.ACTION_SET_EXPAND_DIRECTIONS(false);
    }
  };

  mapFloorTitleToDisplayValue = (floorTitle) => {
    switch (floorTitle) { //TODO move to utility module? Confirm floorTitle for Basement and Mezzanine
      case 'Basement':
        return 'Basement';
      case 'Mezzanine':
        return 'Mezzanine';
      case 'L1':
        return 'Floor 1';
      case 'L2':
        return 'Floor 2';
      case 'L3':
        return 'Floor 3';
      case 'L4':
        return 'Floor 4';
      case 'L5':
        return 'Floor 5';
      default:
        return '';
    }
  };
}

const mapStateToProps = (storeState) => {
  return {
    selectedFloorId: storeState.CURRENT_FLOOR_ID,
    restroomSearched: storeState.RESTROOM_SEARCH
  }
};

export default connect(mapStateToProps, {
  ACTION_SET_FLOOR,
  ACTION_RESET_RESTROOM_SEARCH,
  ACTION_SET_EXPAND_DIRECTIONS
})(FloorSelector)
