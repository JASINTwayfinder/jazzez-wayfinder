import './App.css';
import 'components/RadioButton/RadioButton.css';
import 'components/CheckboxButton/CheckboxButton.css';

import React, { Component } from 'react';
import jmap from 'jmap';
import jmapConfig from 'jmap-config';
import CONFIG from 'config';
import { connect } from 'react-redux';
import {
  ACTION_SET_JIBESTREAM_LOADING,
  ACTION_SET_FLOOR,
  ACTION_SET_VISIBLE_AMENITIES,
  ACTION_SET_IS_KIOSK
} from 'redux/actions';

import WelcomeScreen from 'components/WelcomeScreen/WelcomeScreen';
import LoadingLayover from 'components/LoadingLayover/LoadingLayover';
import Header from 'components/Header/Header';
import Map from 'components/Map/Map';
import Nav from 'components/Nav/Nav';
import PrintPage from 'components/PrintPage/PrintPage';
import StartingPointSelector from 'components/StartingPointSelector/StartingPointSelector';
import OfficeRangeViewModel from 'office-range-view-model';

class App extends Component {

  render = () => {
    return (
      <div>
        {this.displayStartScreen()}
        <header style={{height: this.props.headerHeight}}>
          <Header/>
        </header>
        <main style={{height: this.props.mainHeight}}>
          <Map/>
        </main>
        <nav style={{height: this.props.navHeight}}>
          {this.props.isJibestreamLoading ? '' : <Nav/>}
        </nav>
        <PrintPage />
      </div>
    );
  };

  displayStartScreen = () => {
    if(this.props.isJibestreamLoading){
      return this.props.isKiosk ? <WelcomeScreen/> : <LoadingLayover />
    } else {
      return this.props.isKiosk ? <WelcomeScreen/> : <StartingPointSelector />
    }
  };

  componentDidMount = () => {
    var onMapsRendered = (building) => {
      jmap.clearLocalStorage();
      window.BUILDING = building;
      // TODO uncomment once data is standardized
      window.OFFICES = OfficeRangeViewModel(); //TODO create service instead of global var
      logCMSData();

      this.props.ACTION_SET_VISIBLE_AMENITIES();
      this.props.ACTION_SET_FLOOR(window.BUILDING.currentFloor.id);
      this.props.ACTION_SET_JIBESTREAM_LOADING(false);

      if(this.props.isKiosk){
        setTimeout(() => { // even Jibestream is not sure why this is needed, but it is (lol)
          window.BUILDING.setYah();
        }, 300);
      }
    };

    var deviceId = new URL(window.location.href).searchParams.get('deviceId');
    this.props.ACTION_SET_IS_KIOSK(deviceId !== null);

    jmap.init({
      url: CONFIG.JIBESTREAM.URL,
      id: deviceId || CONFIG.JIBESTREAM.DEFAULT_DEVICE_ID,
      locationId: CONFIG.JIBESTREAM.LOCATION_ID,
      apiUser: CONFIG.JIBESTREAM.API_USER,
      apiPass: CONFIG.JIBESTREAM.API_PASSWORD,
      apiKey: CONFIG.JIBESTREAM.API_KEY,
      code: CONFIG.JIBESTREAM.CODE,
      config: jmapConfig,
      container: "#map",
      allMapsRendered: onMapsRendered
    });
  };
}

function logCMSData() {
  console.log('Devices:');
  console.log(jmap.getDevices());
  console.log('Amenities:');
  console.log(jmap.getAmenities());
  console.log('Destinations:');
  console.log(jmap.getDestinations());
  console.log('You Are Here:');
  console.log(window.BUILDING.destYah);
}

const mapStateToProps = (storeState) => {
  return {
    isJibestreamLoading: storeState.LOADING_JIBESTREAM,
    headerHeight: storeState.HEADER_HEIGHT,
    mainHeight: storeState.MAIN_HEIGHT,
    navHeight: storeState.NAV_HEIGHT,
    footerHeight: storeState.FOOTER_HEIGHT,
    isKiosk: storeState.IS_KIOSK
  }
};

export default connect(mapStateToProps, {
  ACTION_SET_JIBESTREAM_LOADING,
  ACTION_SET_FLOOR,
  ACTION_SET_VISIBLE_AMENITIES,
  ACTION_SET_IS_KIOSK
})(App);
