import './StartingPointSavedLocationsError.css';

import React, {Component} from 'react';

export default class StartingPointSavedLocationsError extends Component {
  render = () => {
    return (
      <div id="starting-point-saved-locations-error" className='starting-point-saved-locations-error'>
        <div className="starting-point-saved-locations-error-info">
          <div className="title">
            You Do Not Have a Saved Location
          </div>
          <p>
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem
            aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
          </p>
        </div>
      </div>
    );
  };

  componentDidMount = () => {};
};
