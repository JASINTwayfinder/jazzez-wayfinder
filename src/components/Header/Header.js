import './Header.css';

import React, {Component} from 'react';
import whsLogo from 'assets/whs-logo.png';
import dodLogo from 'assets/dod-logo.gif';

class Header extends Component {
  render() {
    return (
      <div id="header-container">
        <div className="title">U.S. Department of Defense</div>
        <img className="dodLogo" src={dodLogo} alt="Department of Defense Logo"/>
        <img className="whsLogo" src={whsLogo} alt="Washington Headquarters Services Logo"/>
      </div>
    );
  }
}

export default Header
