import './SearchFilters.css';

import React, {Component} from 'react';
import { connect } from 'react-redux'
import {
  ACTION_SET_SEARCH_FILTER_FLOOR_ID
} from 'redux/actions';

class SearchFilters extends Component {

  render = () => {
    //TODO using fat arrow func in onClick below has bad performance--refactor https://stackoverflow.com/questions/29810914/react-js-onclick-cant-pass-value-to-method
    return (
      <div className="search-filters">
        <span className="title">Filter by floor</span>
        <ul>
          <li>

            <div className="button-radio" onClick={() => this.setFilter(null)} >
              <input type="radio" name="floorFilter" id="all" defaultChecked={true}/>
              <label htmlFor="all">
                <span className="box">
                  <span className="innerBox" />
                </span>
                View All
              </label>
            </div>

          </li>
          {
            Object
              .keys(window.BUILDING.floors)
              .map(e => window.BUILDING.floors[e])
              .sort((a, b) => a.sequence - b.sequence)
              .map((floor, index) => {
                return (
                  <li key={index}>
                    <div className="button-radio" onClick={() => this.setFilter(floor.id)}>
                      <input type="radio" name="floorFilter" id={floor.id}/>
                      <label htmlFor={floor.id}>
                        <span className="box">
                          <span className="innerBox" />
                        </span>
                        {this.mapFloorTitleToDisplayValue(floor.floorTitle)}
                      </label>
                    </div>
                  </li>
                )
              })
          }
        </ul>
      </div>
    );
  };

  setFilter = (floorId) => {
    this.props.ACTION_SET_SEARCH_FILTER_FLOOR_ID(floorId);
  };

  mapFloorTitleToDisplayValue = (floorTitle) => {
    switch (floorTitle) { //TODO move to utility module? Confirm floorTitle for Basement and Mezzanine
      case 'Basement':
        return 'Basement';
      case 'Mezzanine':
        return 'Mezzanine';
      case 'L1':
        return 'Floor One';
      case 'L2':
        return 'Floor Two';
      case 'L3':
        return 'Floor Three';
      case 'L4':
        return 'Floor Four';
      case 'L5':
        return 'Floor Five';
      default:
        return '';
    }
  };

}

export default connect(null, {
  ACTION_SET_SEARCH_FILTER_FLOOR_ID
})(SearchFilters);
