import './ElevatorToggle.css';

import React, {Component} from 'react';
import { connect } from 'react-redux';
import elevatorIcon from 'assets/icons/icon-elevator.svg';
import escalatorIcon from 'assets/icons/icon-escalator.svg';
import {
  ACTION_SET_USE_ELEVATORS,
  ACTION_RESET_DIRECTIONS,
  ACTION_WAYFIND
} from 'redux/actions';

class ElevatorToggle extends Component {

  render = () => {
    return (
      <div className="elevator-toggle">
        <div className={"right " + (this.props.useElevators ? "selected" : "")} onClick={this.useElevators}>
          <img alt="elevator" src={elevatorIcon} />
        </div>
        <div className={"left " + (this.props.useElevators ? "" : "selected")} onClick={this.useEscalators}>
          <img alt="escalator" src={escalatorIcon} />
        </div>
      </div>
    );
  };

  useEscalators = () => {
    if(this.props.useElevators){
      this.props.ACTION_SET_USE_ELEVATORS(false);
      this.props.ACTION_RESET_DIRECTIONS();
      this.props.ACTION_WAYFIND(this.props.currentDest);
    }
  };

  useElevators = () => {
    if(!this.props.useElevators){
      this.props.ACTION_SET_USE_ELEVATORS(true);
      this.props.ACTION_RESET_DIRECTIONS();
      this.props.ACTION_WAYFIND(this.props.currentDest);
    }
  };

}

const mapStateToProps = (storeState) => {
  return {
    useElevators: storeState.USE_ELEVATORS,
    currentDest: storeState.WAYFIND_DEST
  }
};

export default connect(mapStateToProps, {
  ACTION_SET_USE_ELEVATORS,
  ACTION_RESET_DIRECTIONS,
  ACTION_WAYFIND
})(ElevatorToggle)
