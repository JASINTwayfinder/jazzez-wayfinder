import './StartingPointParkingSelector.css';

import React, {Component} from 'react';
import { connect } from 'react-redux'
import StartingPointSearchResults from 'components/StartingPointSearchResults/StartingPointSearchResults';
import {
  ACTION_SET_STARTING_POINT,
  ACTION_SET_SHOW_STARTING_POINT_PARKING_SELECTOR,
  ACTION_STARTING_CATEGORY_SEARCH
} from 'redux/actions';

class StartingPointParkingSelector extends Component {

  state = {
    saveDest: false
  };

  render = () => {
    return (
      <div id="starting-point-parking-search" className='starting-point-parking-search'>
        <div className="btn--top-left" onClick={this.exitSearch}>
          <i className="fa fa-long-arrow-left" aria-hidden="true"/> Back
        </div>
        <div className="starting-point-parking-search-info">
          <div className="title">
            Parking Lots
          </div>
          <p>
            Your trip is starting from a Pentagon Parking Lot. To park on the Pentagon Reservation your vehicle must be
            registered on the Parking Management database and your parking pass must be displayed in your vehicle at
            all times while on the reservation. If your trip requires temporary visitor’s parking be sure to contact
            the Parking Management Office (PMO) 24hrs before your travel.<br/><br/>
            Parking Management Office (PMO) | Pentagon Room 2D1039 | 8:00am – 4:00pm Mon.-Fri. | 703-697-6251
          </p>
          <div className="save-selector">
            <div className="save-checkbox">
              <input type="checkbox"
                     className="button-checkbox"
                     name="saveDest"
                     id="saveDest"
                     checked={this.state.saveDest}
                     onClick={this.toggleSave}/>
              <label htmlFor="saveDest"> Save my starting location</label>
            </div>
          </div>
          <div className="begin">
            <div className="begin-button">
              <button disabled={this.props.selectedParking === null}
                      onClick={this.handleSubmit}>Begin</button>
            </div>
          </div>
        </div>
        <StartingPointSearchResults />
      </div>
    );
  };

  componentDidMount = () => {

  };

  toggleSave = (event) => {
    this.setState({
      saveDest: event.target.checked
    })
  };

  handleSubmit = () => {
    this.props.ACTION_SET_STARTING_POINT(this.props.selectedParking, this.state.saveDest);
  };

  exitSearch = () => {
    this.props.ACTION_SET_SHOW_STARTING_POINT_PARKING_SELECTOR(false);
  };
}

const mapStateToProps = (storeState) => {
  return {
    selectedParking: storeState.STARTING_SEARCH_RESULT
  }
};

export default connect(mapStateToProps, {
  ACTION_SET_STARTING_POINT,
  ACTION_SET_SHOW_STARTING_POINT_PARKING_SELECTOR,
  ACTION_STARTING_CATEGORY_SEARCH
})(StartingPointParkingSelector);
