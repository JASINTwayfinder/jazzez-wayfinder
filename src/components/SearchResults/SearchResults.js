import './SearchResults.css';

import React, {Component} from 'react';
import { connect } from 'react-redux';
import jmap from 'jmap';
import SearchFilters from 'components/SearchFilters/SearchFilters';
import {
  ACTION_SET_FLOOR,
  ACTION_RESET_SEARCH,
  ACTION_SET_SEARCH_FILTER_FLOOR_ID,
  ACTION_SET_EXPAND_NAV,
  ACTION_SET_EXPAND_DIRECTIONS,
  ACTION_SET_TEXT_DIRECTIONS,
  ACTION_SET_DIRECTION_TEXT,
  ACTION_WAYFIND
} from 'redux/actions';

class SearchResults extends Component {

  render() {
    return (
      <div className="search-results">
        <ul className="search-results-list">
          {this.renderResultsList()}
        </ul>
        <SearchFilters />
        <div className="nav--top-right-buttons">
          <i className="fa fa-times-thin" aria-hidden="true" onClick={this.clearSearch}/>
        </div>
      </div>
    );
  }

  renderResultsList = () => {
    //TODO: Remove lambda function in onClick
    var filteredResults = Object
      .keys(this.props.results)
      .map(e => this.props.results[e])
      .filter((item) => {
        return this.props.filter === null || item.mapId === this.props.filter;
      });

    if (filteredResults.length === 0) {
      return <div className="no-matches-found">No Matches Found</div>;
    }

    return filteredResults.map((item, index, results) => {
      var currentFirstLetter = item.name.charAt(0).toUpperCase();
      var newLetter = index > 0 && currentFirstLetter !== results[index-1].name.charAt(0).toUpperCase();
      var bigLetter = index === 0 || newLetter ? item.name.charAt(0).toUpperCase() : "";
      return (
        <div key={index} className={newLetter ? "new-letter" : ""}>
          <div className="divider-wrapper">
            <div className="header"></div>
            <div className="divider"></div>
          </div>
          <li className={index < results.length -1 ? "" : "last-item"}>
            <div className="header">{bigLetter}</div>
            <div className="link" onClick={() => this.wayfindTo(item)}>
              <div className="room"><strong>{item.name}</strong></div>
              <div className="room-number">{this.getFloorText(item)}</div>
            </div>
          </li>
        </div>
      );
    })

  };

  wayfindTo = (item) => {
    this.props.ACTION_WAYFIND(item);
    this.props.ACTION_SET_EXPAND_NAV(false);
    this.props.ACTION_SET_EXPAND_DIRECTIONS(true);
  };

  getFloorText = (result) => {
    var floor = jmap.getFloorDataByMapId(result.mapId);
    switch (floor.name) { //TODO move to utility module? Confirm floorTitle for Basement and Mezzanine
      case 'Basement':
        return 'Basement';
      case 'Mezzanine':
        return 'Mezzanine';
      default:
        return 'Floor ' + floor.name.substr(1);
    }
  };

  clearSearch = () => {
    this.props.ACTION_RESET_SEARCH();
    this.props.ACTION_SET_EXPAND_NAV(false);
    this.props.ACTION_SET_EXPAND_DIRECTIONS(false);
  };

  componentDidMount = () => {
    this.search = '';
  };
}

const mapStateToProps = (storeState) => {
  return {
    results: storeState.SEARCH_RESULTS,
    filter: storeState.SEARCH_FILTER_FLOOR_ID
  }
};

export default connect(mapStateToProps, {
  ACTION_SET_FLOOR,
  ACTION_RESET_SEARCH,
  ACTION_SET_SEARCH_FILTER_FLOOR_ID,
  ACTION_SET_EXPAND_NAV,
  ACTION_SET_EXPAND_DIRECTIONS,
  ACTION_SET_TEXT_DIRECTIONS,
  ACTION_SET_DIRECTION_TEXT,
  ACTION_WAYFIND
})(SearchResults);
