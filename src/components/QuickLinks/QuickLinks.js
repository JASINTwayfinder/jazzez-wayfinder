import './QuickLinks.css';

import React, {Component} from 'react';
import { connect } from 'react-redux'

import bathroomIcon from 'assets/icons/icon-bathrooms.svg';
import foodIcon from 'assets/icons/icon-food.svg';
import exhibitIcon from 'assets/icons/icon-exhibit.svg';
import chapelIcon from 'assets/icons/icon-chapel.svg';
import exitIcon from 'assets/icons/icon-exits.svg';
import servicesIcon from 'assets/icons/icon-services.svg';
import retailIcon from 'assets/icons/shoppingbag-icon.svg';
import parkingIcon from 'assets/icons/icon-parking.svg';
import officeIcon from 'assets/icons/icon-offices.svg';
import gymIcon from 'assets/icons/icon-gym.svg';
import eventSpacesIcon from 'assets/icons/icon-eventsspaces.svg';
import rightArrowIcon from 'assets/icons/icon-right.svg';
import leftArrowIcon from 'assets/icons/icon-left.svg';

import {
  ACTION_SET_FLOOR,
  ACTION_CATEGORY_SEARCH,
  ACTION_RESTROOM_SEARCH,
  ACTION_SET_EXPAND_NAV,
  ACTION_SET_EXPAND_ROOM_SEARCH,
  ACTION_SET_EXPAND_DIRECTIONS,
  ACTION_NEAREST_DESTINATION_SEARCH
} from 'redux/actions';

class QuickLinks extends Component {
  state = {
    showArrowLeft: false,
    showArrowRight: false,
    spaceAround: false
  };
  serviceSecondaryCategories = [
    'personalized service',
    'business services',
    'banking services',
    'atm'
  ];
  parkingSecondaryCategories = [
    "parking"
  ];

  render = () => {
    return (
      <div
        id="quick-links"
        ref={e => this.quickLinksElement = e}
        onScroll={this.determineArrowVisibility}
        className={'quick-links ' + (this.state.showArrowLeft ? 'fade-left ' : '') + (this.state.showArrowRight ? 'fade-right ' : '')}
      >
        <ul className="amenity-buttons" style={{justifyContent: this.state.spaceAround ? 'space-around' : ''}}>

          <span className="direction-arrow left"
                style={{visibility: this.state.showArrowLeft ? 'visible' : 'hidden'}}
                onClick={this.leftArrowClicked}>
            <img alt="arrow-left" src={leftArrowIcon} />
          </span>
          <span className="direction-arrow right"
                style={{visibility: this.state.showArrowRight ? 'visible' : 'hidden'}}
                onClick={this.rightArrowClicked}>
            <img alt="arrow-right" src={rightArrowIcon} />
          </span>

          <li className="amenity-button" ref={e => this.firstButton = e} onClick={this.showRoomSearch}>
            <img alt="offices" src={officeIcon} />
            <div>Rooms</div>
          </li>
          <li className="amenity-button" onClick={() => this.restroomSearch("Restroom")}>
            <img alt="restroom" src={bathroomIcon} />
            <div>Bathrooms</div>
          </li>
          <li className="amenity-button" onClick={() => this.categorySearch("food beverage")}>
            <img alt="food and drink" src={foodIcon} />
            <div>Food</div>
          </li>
          <li className="amenity-button" onClick={() => this.categorySearch("service atm", this.serviceSecondaryCategories)}>
            <img alt="services" src={servicesIcon} />
            <div>Services</div>
          </li>
          <li className="amenity-button" onClick={() => this.categorySearch("retail")}>
            <img alt="retail" src={retailIcon} />
            <div>Retail</div>
          </li>
          <li className="amenity-button" onClick={() => this.nearestSearch("athletic center")}>
            <img alt="gym" src={gymIcon} />
            <div>Gym</div>
          </li>
          <li className="amenity-button" onClick={() => this.categorySearch("exhibit")}>
            <img alt="exhibits" src={exhibitIcon} />
            <div>Exhibits</div>
          </li>
          <li className="amenity-button" onClick={() => this.categorySearch("event")}>
            <img alt="event spaces" src={eventSpacesIcon} />
            <div>Event Spaces</div>
          </li>
          <li className="amenity-button" onClick={() => this.nearestSearch("chapel")}>
            <img alt="chapel" src={chapelIcon} />
            <div>Chapel</div>
          </li>
          <li className="amenity-button" onClick={() => this.categorySearch("parking", this.parkingSecondaryCategories)}>
            <img alt="parking" src={parkingIcon} />
            <div>Parking</div>
          </li>
          <li className="amenity-button"
              onClick={() => this.categorySearch("entrance exit")}
              ref={e => this.lastButton = e}>
            <img alt="exits" src={exitIcon} />
            <div>Entrances & Exits</div>
          </li>
        </ul>
      </div>
    );
  };

  componentDidMount = () => {
    this.determineArrowVisibility();
    window.addEventListener('resize', this.determineArrowVisibility);
  };

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.determineArrowVisibility);
  };

  restroomSearch = (category) => {
    this.props.ACTION_RESTROOM_SEARCH(category);
    this.props.ACTION_SET_EXPAND_DIRECTIONS(true);
  };

  nearestSearch = (category) => {
    this.props.ACTION_NEAREST_DESTINATION_SEARCH(category);
    this.props.ACTION_SET_EXPAND_DIRECTIONS(true);
  };

  categorySearch = (category, secondaryCategories = []) => {
    this.props.ACTION_CATEGORY_SEARCH(category, secondaryCategories);
    this.props.ACTION_SET_EXPAND_NAV(true);
  };

  showRoomSearch = () =>{
    this.props.ACTION_SET_EXPAND_ROOM_SEARCH(true);
  };

  determineArrowVisibility = () => {
    var containerRect = this.quickLinksElement.getBoundingClientRect();
    var firstButtonRect = this.firstButton.getBoundingClientRect();
    var lastButtonRect = this.lastButton.getBoundingClientRect();

    var showArrowLeft = firstButtonRect.left < containerRect.left;
    var showArrowRight = Math.floor(lastButtonRect.right) > containerRect.right;

    this.setState({showArrowLeft});
    this.setState({showArrowRight});
    this.setState({spaceAround: !showArrowLeft && !showArrowRight});
  };

  leftArrowClicked = () => {
    this.quickLinksElement.scrollLeft += -this.quickLinksElement.clientWidth;
    this.determineArrowVisibility();
  };

  rightArrowClicked = () => {
    this.quickLinksElement.scrollLeft += this.quickLinksElement.clientWidth;
    this.determineArrowVisibility();
  };
}

const mapStateToProps = (storeState) => {
  return {
    category: storeState.SEARCH_CATEGORY
  }
};

export default connect(mapStateToProps, {
  ACTION_SET_FLOOR,
  ACTION_CATEGORY_SEARCH,
  ACTION_RESTROOM_SEARCH,
  ACTION_SET_EXPAND_NAV,
  ACTION_SET_EXPAND_ROOM_SEARCH,
  ACTION_SET_EXPAND_DIRECTIONS,
  ACTION_NEAREST_DESTINATION_SEARCH
})(QuickLinks);
