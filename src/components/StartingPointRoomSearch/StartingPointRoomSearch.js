import './StartingPointRoomSearch.css';

import React, {Component} from 'react';
import StartingPointRoomSearchForm from 'components/StartingPointRoomSearchForm/StartingPointRoomSearchForm';
import { connect } from 'react-redux'
import numberExplanation from 'assets/number-explanation.png';
import {
  ACTION_SET_SHOW_STARTING_POINT_ROOM_SEARCH
} from 'redux/actions';

class StartingPointRoomSearch extends Component {

  render = () => {
    return (
      <div id="starting-point-room-search" className='starting-point-room-search'>
        <div className="btn--top-left" onClick={this.exitSearch}>
          <i className="fa fa-long-arrow-left" aria-hidden="true"/> Back
        </div>
        <div className="starting-point-room-search-info">
          <div className="title">
            Rooms
          </div>
          <p>
            Your trip is starting from an office inside the Pentagon. Select the floor, ring, corridor and office area
            range to confirm the proximity of your starting point. The office area range can be determined by selecting
            the number grouping closest to your starting point room number.
          </p>
          <div className="room-image">
            <img alt="Room number description" src={numberExplanation} />
          </div>
        </div>
        <StartingPointRoomSearchForm />
      </div>
    );
  };

  exitSearch = () => {
    this.props.ACTION_SET_SHOW_STARTING_POINT_ROOM_SEARCH(false);
  };
}

const mapStateToProps = (storeState) => {
  return {}
};

export default connect(mapStateToProps, {
  ACTION_SET_SHOW_STARTING_POINT_ROOM_SEARCH
})(StartingPointRoomSearch);
