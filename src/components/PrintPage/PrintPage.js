import './PrintPage.css';

import React, {Component} from 'react';
import { connect } from 'react-redux';

class PrintPage extends Component {

  render = () => {
    return (
      <div id="print-page">
        <ol>
          {this.props.textDirections.map((textDirection, index) => {
            return <li key={index}>{textDirection.output}</li>
          })}
        </ol>
      </div>
    );
  };

}

const mapStateToProps = (storeState) => {
  return {
    textDirections: storeState.TEXT_DIRECTIONS
  }
};

export default connect(mapStateToProps)(PrintPage);
