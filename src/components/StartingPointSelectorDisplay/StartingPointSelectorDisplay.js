import './StartingPointSelectorDisplay.css';

import React, {Component} from 'react';
import { connect } from 'react-redux';
import exitIcon from 'assets/icons/icon-exits.svg';
import savedIcon from 'assets/icons/icon-saved.svg';
import parkingIcon from 'assets/icons/icon-parking.svg';
import officeIcon from 'assets/icons/icon-offices.svg';
import {
  ACTION_STARTING_CATEGORY_SEARCH,
  ACTION_SET_STARTING_SEARCH_RESULTS_SAVED_LOCATIONS,
  ACTION_SET_SHOW_STARTING_POINT_ROOM_SEARCH,
  ACTION_SET_SHOW_STARTING_POINT_PARKING_SELECTOR,
  ACTION_SET_SHOW_STARTING_POINT_ENTRANCE_SELECTOR,
  ACTION_SET_SHOW_STARTING_POINT_SAVED_LOCATIONS
} from 'redux/actions';

class StartingPointSelectorDisplay extends Component {


  render = () => {
    return (
      <div className="selector-display">
        <div className="title">Choose Your Starting Point</div>
        <p>
          Navigating the Pentagon just got easier! Now you can plan your trip to your desired location(s) in the
          Pentagon right from your workstation. Let get oriented. Choose a starting point from the icons below.
          <br/><br/>
          Questions about Pentagon access?
          <br/>
          Pentagon employees who have a DoD Common Access Card or PFAC may request unescorted access ("No Escort
          Required") for visitors who need infrequent but regular access to the Pentagon, for official business only,
          and up to six months.
          <br/><br/>
          Government and military DoD CAC holders are not required to have an escort
          but are still encouraged to register prior to their visit to minimize processing times.
          <br/><br/>
          Contact the Pentagon Building Pass Office at (703) 693-3953 with any questions.
          <br/>
          Office hours are Mon., Tue., Thur., and Fri., 8 am-4 pm or Wed. 8:30 am-4 pm
        </p>
        <div className="links">
          <li className="link-button" onClick={this.showOfficeSearch}>
            <img alt="offices" src={officeIcon} />
            <div>Rooms</div>
          </li>
          <li className="link-button" onClick={this.showParkingSearch}>
            <img alt="parking" src={parkingIcon} />
            <div>Parking</div>
          </li>
          <li className="link-button" onClick={this.showEntranceSearch}>
            <img alt="entrances" src={exitIcon} />
            <div>Entrances</div>
          </li>
          <li className="link-button" onClick={this.showSavedLocations}>
            <img alt="saved locations" src={savedIcon} />
            <div>Saved Locations</div>
          </li>
        </div>
      </div>
    );
  };

  showOfficeSearch = () => {
    this.props.ACTION_SET_SHOW_STARTING_POINT_ROOM_SEARCH(true);
  };

  showParkingSearch = () => {
    this.props.ACTION_STARTING_CATEGORY_SEARCH('parking', ['parking']);
    this.props.ACTION_SET_SHOW_STARTING_POINT_PARKING_SELECTOR(true);
  };

  showEntranceSearch = () => {
    this.props.ACTION_STARTING_CATEGORY_SEARCH('entrance exit');
    this.props.ACTION_SET_SHOW_STARTING_POINT_ENTRANCE_SELECTOR(true);
  };

  showSavedLocations = () => {
    this.props.ACTION_SET_STARTING_SEARCH_RESULTS_SAVED_LOCATIONS();
    this.props.ACTION_SET_SHOW_STARTING_POINT_SAVED_LOCATIONS(true);
  };

  componentDidMount = () => {};
}

const mapStateToProps = (storeState) => {
  return {
    display: storeState.DISPLAY_STARTING_POINT_SELECTOR
  }
};

export default connect(mapStateToProps, {
  ACTION_STARTING_CATEGORY_SEARCH,
  ACTION_SET_STARTING_SEARCH_RESULTS_SAVED_LOCATIONS,
  ACTION_SET_SHOW_STARTING_POINT_ROOM_SEARCH,
  ACTION_SET_SHOW_STARTING_POINT_PARKING_SELECTOR,
  ACTION_SET_SHOW_STARTING_POINT_ENTRANCE_SELECTOR,
  ACTION_SET_SHOW_STARTING_POINT_SAVED_LOCATIONS
})(StartingPointSelectorDisplay);
