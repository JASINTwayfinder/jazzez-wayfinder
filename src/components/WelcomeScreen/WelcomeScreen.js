import './WelcomeScreen.css';

import React, {Component} from 'react';
import CONFIG from 'config';
import { connect } from 'react-redux'
import jmap from 'jmap';
import {
  ACTION_SET_DISPLAY_WELCOME_SCREEN,
  ACTION_RESET_SEARCH,
  ACTION_SET_EXPAND_NAV,
  ACTION_SET_EXPAND_DIRECTIONS,
  ACTION_SET_EXPAND_ROOM_SEARCH,
  ACTION_SET_WAYFIND_DEST,
  ACTION_SET_USE_ELEVATORS,
  ACTION_SET_FLOOR
} from 'redux/actions';

class WelcomeScreen extends Component {

  render = () => {
    if (this.props.display) {
      return (
        <div className="welcome-screen" onClick={this.hideWelcomeScreen}>
          Touch to begin
        </div>
      );
    } else {
      return null;
    }
  };

  componentDidMount = () => {
    var t;
    var resetTimer = () => {
      clearTimeout(t);
      t = setTimeout(() => {
        this.props.ACTION_RESET_SEARCH();
        this.props.ACTION_SET_EXPAND_NAV(false);
        this.props.ACTION_SET_EXPAND_ROOM_SEARCH(false);
        this.props.ACTION_SET_EXPAND_DIRECTIONS(false);
        this.props.ACTION_SET_USE_ELEVATORS(false);
        this.props.ACTION_SET_WAYFIND_DEST(null);
        this.props.ACTION_SET_FLOOR(jmap.getFloorDataByMapId(window.BUILDING.destYah.mapId).id);
        this.props.ACTION_SET_DISPLAY_WELCOME_SCREEN(true);
      }, CONFIG.WELCOME_SCREEN_TIMEOUT*60*1000);
    };
    document.onload = resetTimer;
    document.onclick = resetTimer;      // touchpad clicks
    document.onmousedown = resetTimer;  // touchscreen presses
    document.ontouchstart = resetTimer;
  };

  hideWelcomeScreen = () => {
    this.props.ACTION_SET_DISPLAY_WELCOME_SCREEN(false);
  };
}

const mapStateToProps = (storeState) => {
  return {
    display: storeState.DISPLAY_WELCOME_SCREEN
  }
};

export default connect(mapStateToProps, {
  ACTION_SET_DISPLAY_WELCOME_SCREEN,
  ACTION_RESET_SEARCH,
  ACTION_SET_EXPAND_NAV,
  ACTION_SET_EXPAND_DIRECTIONS,
  ACTION_SET_EXPAND_ROOM_SEARCH,
  ACTION_SET_WAYFIND_DEST,
  ACTION_SET_USE_ELEVATORS,
  ACTION_SET_FLOOR
})(WelcomeScreen);
