import './RoomSearch.css';

import React, {Component} from 'react';
import RoomSearchForm from 'components/RoomSearchForm/RoomSearchForm';
import { connect } from 'react-redux'
import numberExplanation from 'assets/number-explanation.png';
import {
  ACTION_SET_EXPAND_ROOM_SEARCH
} from 'redux/actions';

class RoomSearch extends Component {

  render = () => {
    return (
      <div id="room-search" className='room-search'>
        <div className="room-search-info">
          <div className="title">
            Find Your Room
          </div>
          <p>
            Welcome! This tool is designed to help you navigate the Pentagon.
            Routing your office area will place you in close proximity to your desired destination.
            Simply select the floor, ring, corridor, office area range of your destination and press begin.
          </p>
          <div className="room-image">
            <img alt="Room number description" src={numberExplanation} />
          </div>
        </div>
        <RoomSearchForm />
        <div className="nav--top-right-buttons">
          <i className="fa fa-times-thin" aria-hidden="true" onClick={this.exitSearch}/>
        </div>
      </div>
    );
  };

  exitSearch = () => {
    this.props.ACTION_SET_EXPAND_ROOM_SEARCH(false);
  };
}

const mapStateToProps = (storeState) => {
  return {
    roomSearchExpanded: storeState.ROOM_SEARCH_EXPANDED,
  }
};

export default connect(mapStateToProps, {
  ACTION_SET_EXPAND_ROOM_SEARCH
})(RoomSearch);
