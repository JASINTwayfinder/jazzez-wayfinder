import './Dropdown.css';

import React, {Component} from 'react';
import FontAwesome from 'react-fontawesome';

class Dropdown extends Component {

  state = {
    show: false
  };

  render = () => {
    var buttonText = this.props.value === '' ? this.props.placeholder : this.props.value;
    var buttonClass = this.props.disabled ? "dropdown-button disabled" : "dropdown-button";
    var dropdownClass = this.state.show ? "dropdown-content show" : "dropdown-content";
    dropdownClass += this.props.dropUp ? " dropUp" : " test";

    var dropdown = (
      <div id="myDropdown" className={dropdownClass}>
        {this.props.children}
      </div>
    );

    return (
      <div className="dropdown" tabIndex="0" onClick={this.toggleDropdown} onBlur={this.hideDropdown}>
        {this.props.dropUp ? dropdown : ''}
        <div className={buttonClass}>
          <span className="text">{buttonText}</span>
          <span className="arrow">
            <FontAwesome name="angle-down"/>
          </span>
        </div>
        {this.props.dropUp ? '' : dropdown}
      </div>
    );
  };

  onClick = () => {};

  toggleDropdown = (e) => {
    e.preventDefault();
    e.target.focus();
    if(!this.props.disabled){
      this.setState({show: !this.state.show})
    }
  };

  hideDropdown = () => {
    this.setState({show: false});
  };

  componentDidMount = () => {};
}

Dropdown.defaultProps = {
  disabled: false,
  dropUp: false
};

export default Dropdown;
