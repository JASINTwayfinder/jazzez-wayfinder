import './LoadingLayover.css';

import React, {Component} from 'react';
import poweredByInfoNetLogo from 'assets/LogoInfoNet.svg';

export default class LoadingLayover extends Component {
  render = () => {
    return (
      <div className="loading-layover" >
        <div className="infonet-logo">
          <img alt="Powered by InfoNet logo" src={poweredByInfoNetLogo}/>
        </div>
        <div className="loader-container">
          <div className="loader" />
        </div>
      </div>
    );
  };

}
