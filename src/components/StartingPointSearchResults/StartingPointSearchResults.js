import './StartingPointSearchResults.css';

import React, {Component} from 'react';
import { connect } from 'react-redux'
import {
  ACTION_SET_STARTING_SEARCH_RESULT
} from 'redux/actions';

class StartingPointSearchResults extends Component {

  state = {
    selectedOption: null
  };

  render = () => {
    return (
      <div className="starting-point-search-results">
        <ul>
          {this.renderOptions()}
        </ul>
      </div>
    );
  };

  renderOptions = () => {
    return Object
      .keys(this.props.results)
      .map((e, index) => {
        var item = this.props.results[e];
        return (
          <li key={index} onClick={() => this.selectOption(item)}>
            <div className="button-radio">
              <input type="radio" name="parkingSelector" id={index}/>
              <label htmlFor={index}>
                <span className="box">
                  <span className="innerBox" />
                </span>
                {item.name}
              </label>
            </div>
          </li>
        )
      });
  };

  componentDidMount = () => {};

  selectOption = (dest) => {
    this.props.ACTION_SET_STARTING_SEARCH_RESULT(dest);
  };

}

const mapStateToProps = (storeState) => {
  return {
    results: storeState.STARTING_SEARCH_RESULTS
  }
};

export default connect(mapStateToProps, {
  ACTION_SET_STARTING_SEARCH_RESULT
})(StartingPointSearchResults);
