import './StartingPointRoomSearchForm.css';

import React, {Component} from 'react';
import { connect } from 'react-redux';
import Dropdown from 'components/Dropdown/Dropdown';
import {
  ACTION_SET_STARTING_POINT_ROOM,
  ACTION_SAVE_WAYPOINT
} from 'redux/actions';

class StartingPointRoomSearchForm extends Component {

  roomData = window.OFFICES;
  state = {
    saveDest: false,
    floor: "",
    ring: "",
    corridor: "",
    areaString: "",
    roomDataIndex: "",
    officeAreaId: ""
  };

  render = () => {
    return (
      <div className="starting-point-room-search-form">
        <form>
          <div>
            <span className="dropdown-container">
              <Dropdown value={this.state.floor}
                        placeholder="Floor">
                {
                  this.roomData
                    .reduce((prev, item) => {
                      return prev.indexOf(item.floor) > -1 ? prev : prev.concat(item.floor);
                    }, [])
                    .sort((a, b) => {
                      return this.handleSort(a.toUpperCase(), b.toUpperCase());
                    })
                    .map((floor, index) => {
                      return <div key={index} value={floor} name="floor" onClick={this.handleChange}>{floor}</div>
                    })
                }
              </Dropdown>
            </span>
          </div>
          <div>
            <span className="dropdown-container">
              <Dropdown disabled={this.state.floor === ""}
                        value={this.state.ring}
                        placeholder="Ring">
                {
                  this.roomData
                    .reduce((prev, item) => {
                      if(prev.indexOf(item.ring) === -1 && item.floor === this.state.floor){
                        return prev.concat(item.ring);
                      } else {
                        return prev
                      }
                    }, [])
                    .sort((a, b) => {
                      return this.handleSort(a.toUpperCase(), b.toUpperCase());
                    })
                    .map((ring, index) => {
                      return <div key={index} value={ring} name="ring" onClick={this.handleChange}>{ring}</div>
                    })
                }
              </Dropdown>
            </span>
          </div>
          <div>
            <span className="dropdown-container">
              <Dropdown disabled={this.state.floor === "" || this.state.ring === ""}
                        value={this.state.corridor}
                        placeholder="Corridor">
                {
                  this.roomData
                    .reduce((prev, item) => {
                      if(prev.indexOf(item.corridor) === -1
                        && item.floor === this.state.floor
                        && item.ring === this.state.ring){
                        return prev.concat(item.corridor);
                      } else {
                        return prev
                      }
                    }, [])
                    .sort((a, b) => parseInt(a, 10) - parseInt(b, 10))
                    .map((corridor, index) => {
                      return <div key={index} name="corridor" onClick={this.handleChange} value={corridor}>{corridor}</div>
                    })
                }
              </Dropdown>
            </span>
          </div>
          <div>
            <span className="dropdown-container">
              <Dropdown disabled={this.state.floor === "" || this.state.ring === "" || this.state.corridor === ""}
                        value={this.state.areaString}
                        className="officeArea"
                        placeholder="Office Area">
                {
                  this.roomData
                    .reduce((prev, item, index) => {
                      var areaString = item.officeRange.min + "-" + item.officeRange.max;
                      if(prev.filter((e) => e.areaString === areaString).length <= 0
                        && item.floor === this.state.floor
                        && item.ring === this.state.ring
                        && item.corridor === this.state.corridor){
                        return prev.concat({areaString: areaString, officeRange: item.officeRange, id: item.id, roomDataIndex: index});
                      } else {
                        return prev
                      }
                    }, [])
                    .sort((a, b) => {
                      return this.handleSort(a.areaString.toUpperCase(), b.areaString.toUpperCase());
                    })
                    .map((item, index) => {
                      return <div key={index} name={item.areaString} value={item.roomDataIndex} onClick={this.handleOfficeAreaChange}>{item.areaString}</div>
                    })
                }
              </Dropdown>
            </span>
          </div>
          <div>
            <div className="save-checkbox">
              <input type="checkbox"
                     className="button-checkbox"
                     name="saveDest"
                     id="saveDest"
                     checked={this.state.saveDest}
                     onClick={this.toggleSave}/>
              <label htmlFor="saveDest"> Save my starting location</label>
            </div>
          </div>
          <div>
            <div className="room-search-button">
              <button disabled={this.state.floor === "" || this.state.ring === "" || this.state.corridor === "" || this.state.officeAreaId === ""}
                      onClick={this.handleSubmit}>Begin</button>
            </div>
          </div>
        </form>
      </div>
    );
  };

  handleSort = (a, b) => {
    if(a > b) return 1;
    if(a < b) return -1;
    return 0;
  };

  handleSubmit = (event) => {
    if(this.state.floor !== "" &&
      this.state.ring !== "" &&
      this.state.corridor !== "" &&
      this.state.officeAreaId!== ""){
      this.props.ACTION_SET_STARTING_POINT_ROOM(
        parseInt(this.state.officeAreaId, 10),
        "Office Range " + this.state.floor + this.state.ring + this.state.corridor + this.state.areaString,
        this.state.saveDest);
    } else {
      throw new Error('Room Data Incomplete');
    }
    event.preventDefault();
  };

  toggleSave = (event) => {
    this.setState({
      saveDest: event.target.checked
    })
  };

  handleChange = (event) => {
    // Logic to reset values of lower priority selections + set state
    var resetState = {};
    var name = event.target.attributes.name.value;
    switch(name) {
      case 'floor':
        resetState = {
          ring: "",
          corridor: "",
          areaString: "",
          roomDataIndex: "",
          officeAreaId: ""
        };
        break;
      case 'ring':
        resetState = {
          corridor: "",
          areaString: "",
          roomDataIndex: "",
          officeAreaId: ""
        };
        break;
      case 'corridor':
        resetState = {
          areaString: "",
          roomDataIndex: "",
          officeAreaId: ""
        };
        break;
      default:
        break;
    }
    var state = Object.assign({
      [name]: event.target.attributes.value.value
    }, resetState);
    this.setState(state);
  };

  handleOfficeAreaChange = (event) => {
    // Separated to prevent ID overlap
    var attr = event.target.attributes;
    var state = Object.assign({
      areaString: attr.name.value,
      roomDataIndex: attr.value.value,
      officeAreaId: this.roomData[parseInt(attr.value.value, 10)].id
    });
    this.setState(state);
  };
}

export default connect(null, {
  ACTION_SET_STARTING_POINT_ROOM,
  ACTION_SAVE_WAYPOINT
})(StartingPointRoomSearchForm);
