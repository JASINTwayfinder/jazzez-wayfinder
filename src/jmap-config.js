import iconYah from 'assets/icons/icon-yah-red.svg';
import iconPin from 'assets/icons/icon-marker.svg';

export default {
  "mapStyles": {
    "mapConfig": {
      "startState": {
        "zoom": 1.5,
        "focusSpeed": 1,
        "animateInitialState": true
        //"animateZoom": 10
      },
      "engineOptions": {
        "rotation": false,
        //"rotationBuffer": 7,
        "scale": true,
        "panning": true,
        "boundLimits": true,
        "maxScale": 10,
        "minScale": 1
      },
      //"container": {
      //  "width": "100%",
      //  "height": "100%",
      //  "zoomMax": 800,
      //  "zoomMin": 0,
      //  "iconSwapZoomRatio": 4,
      //  "background": "rgba(0,0,0,0)"
      //},
      //"positionOffset": {
      //  "x": 0,
      //  "y": 0
      //},
      //"floorSwitch": {
      //  "speed": 0.7
      //},
      //"mapLabels": {
      //  "showLogos": true,
      //  "showLabels": true,
      //  "storeLabelStyle": "flag",
      //  "storeLogoSponsoredRating": 50,
      //  "inlayLabels": {
      //    "initialOpacity": 1,
      //    "useLBoxData": true
      //  },
      //  "unitLogos": {
      //    "alwaysHeadsUp": true,
      //    "maxWidth": "auto",
      //    "minWidth": "auto"
      //  }
      //}
    },
    "exportStyles": {
      "Background-Layer": "show",
      "Parking-Lots-Layer": "show",
      "Mall-Boundary-Layer": "show",
      "Streets-SmallAlleys-Layer": "hide",
      "Streets-Minor-Layer": "hide",
      "Streets-Major-Layer": "hide",
      "Obstacles-Layer": "hide",
      "Units-Layer": "hide",
      "Elevators-Layer": "show",
      "Escalators-Layer": "show",
      "LBox-Layer": "hide"
    },
    "mapLayers": [
      {
        "name": "Units-Layer",
        "fill": "#119da4",
        "stroke": "#040404",
        "strokeWidth": ".5",
        "vectorEffect": "non-scaling-stroke",
        "clickable": "true"
      },
      {
        "name": "Elevators-Layer",
        "fill": "#ffffff",
        "stroke": "#040404",
        "strokeWidth": ".5",
        "vectorEffect": "non-scaling-stroke",
        "display": "block"
      },
      {
        "name": "Escalators-Layer",
        "fill": "#ffffff",
        "stroke": "#040404",
        "strokeWidth": ".5",
        "vectorEffect": "non-scaling-stroke",
        "display": "block"
      },
      {
        "name": "Obstacles-Layer",
        "fill": "none",
        "strokeWidth": "0",
        "vectorEffect": "non-scaling-stroke",
        "opacity": ".1"
      },
      {
        "name": "Streets-Major-Layer",
        "fill": "none",
        "stroke": "#717171",
        "strokeWidth": "80",
        "vectorEffect": "non-scaling-stroke",
        "strokeLinejoin": "round",
        "strokeMiterlimit": "10",
        "opacity": "100"
      },
      {
        "name": "Streets-Minor-Layer",
        "fill": "none",
        "stroke": "none",
        "strokeWidth": "40",
        "vectorEffect": "non-scaling-stroke",
        "strokeLinejoin": "round",
        "strokeMiterlimit": "10",
        "opacity": "0"
      },
      {
        "name": "Streets-SmallAlleys-Layer",
        "fill": "none",
        "stroke": "#717171",
        "strokeWidth": "3",
        "vectorEffect": "non-scaling-stroke",
        "strokeLinejoin": "round",
        "strokeMiterlimit": "10"
      },
      {
        "name": "Boundary-Layer",
        "fill": "#040404"
      },

      {
        "name":"Mall-Boundary-Layer",
        "stroke": "#040404",
        "strokeWidth": "2",
        "fill": "#e8e8e8"
      },


      {
        "name": "Corridor-Indoor-Layer",
        "fill": "none"
      },


      {
        "name": "Background-Layer",
        "fill": "#c3c3c3"
      },
      {
        "name": "Restrooms-Layer",
        "fill": "#0f59a4",
        "stroke": "#040404",
        "strokeWidth": ".5"
      },
      /*{
       "name": "Pattern-OutdoorTerrace-Layer",
       "fill": "red",
       "stroke": "#red",
       "strokeWidth": "10",
       "strokeLinejoin": "round",
       "strokeMiterlimit": "0"
       },*/
      {
        "name": "Pattern-Grass-Layer",
        "fill": "#c3c3c3"
      },
      {
        "name": "Pattern-Water-Layer",
        "fill": "#13505b"
      },
      {
        "name": "Parking-Lots-Layer",
        "fill": "#909090"
      },
      {
       "name": "Interior-ParkingLots-Layer",
       "fill": "#909090"
       },
      {
        "name": "Stairs-Layer",
        "display": "block",
        "fill": "#ffffff",
        "stroke": "#040404",
        "strokeWidth": ".5"
      },
      /*{
       "name": "Furniture-Layer",
       "fill": "#b6acc1"
       },*/
      {
        "name": "CustomArtLayer-1-Layer",
        "fill": "#e8e8e8",
        "stroke": "#040404",
        "strokeWidth": ".5"
      },
      {
        "name": "CustomArtLayer-2-Layer",
        "fill": "none"
      },
      /*{
       "name": "CustomArtLayer-3-Layer",
       "fill": "none"
       },*/
      {
        "name": "CustomArtLayer-4-Layer",
        "fill": "none"
      },
      {
        "name": "CustomArtLayer-5-Layer",
        "fill": "#e8e8e8"
      }
    ],
    "pathStyles":{
      "pathType": "line",
      "spacing": 4,
      "pathWidth": 3,
      "pathOpacity": 0.75,
      "pathColor": "#E04151"
    },
    "iconStyles": {
      "youarehere": {
        "url": iconYah,
        "width": 40,
        "height": 40,
        "allowZoomScale": false,
        "offset": {
          "x": -20,
          "y": -40
        }
      },
      "destination": {
        "url": iconPin,
        "width": 40,
        "height": 40,
        "allowZoomScale": false
      }
    }


  }
}
